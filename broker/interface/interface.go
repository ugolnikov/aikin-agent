package broker

type Sender interface {
	Send(message string)
	Close()
}
