package broker

import (
	"aikin-agent/broker/interface"
	"github.com/Shopify/sarama"
	"log"
	"time"
)

type KafkaBroker struct {
	broker.Sender
	kafkaClient   *sarama.Client
	kafkaProducer *sarama.SyncProducer
}

func (broker KafkaBroker) Send(message string) {
	broker.kafkaProduce("agent", message)
}

func NewBroker(host string) broker.Sender {
	b := KafkaBroker{}

	if b.kafkaClient == nil || b.kafkaProducer == nil {
		go func() {
			b.initClient(host)
			b.initProducer()
		}()
	}
	return b
}

func (broker KafkaBroker) Close() {
	if broker.kafkaClient != nil && !(*broker.kafkaClient).Closed() {
		err := (*broker.kafkaClient).Close()
		if err != nil {
			log.Println("Failed to close kafka client:", err)
		}
	}
	if broker.kafkaProducer != nil {
		err := (*broker.kafkaProducer).Close()
		if err != nil {
			log.Println("Failed to close kafka producer:", err)
		}
	}
}

func (broker KafkaBroker) initClient(host string) {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5
	config.Producer.Return.Successes = true

	brokerList := []string{host}
	for {
		c, err := sarama.NewClient(brokerList, config)
		if err != nil {
			log.Println("Failed to create client:", err)
			time.Sleep(5 * time.Second)
		} else {
			broker.kafkaClient = &c
			break
		}
	}
}

func (broker KafkaBroker) initProducer() {
	producer, err := sarama.NewSyncProducerFromClient(*broker.kafkaClient)
	if err != nil {
		log.Println("Failed to create producer:", err)
	} else {
		broker.kafkaProducer = &producer
	}
}

func (broker KafkaBroker) kafkaProduce(topic, message string) {
	msg := &sarama.ProducerMessage{
		Topic:     topic,
		Partition: -1,
		Value:     sarama.StringEncoder(message),
	}

	if broker.kafkaClient != nil && broker.kafkaProducer != nil {
		partition, offset, err := (*broker.kafkaProducer).SendMessage(msg)
		if err != nil {
			log.Println("Failed to send message:", err)
		}
		log.Printf("Message sent to partition %d at offset %d\n", partition, offset)
	} else {
		log.Printf("Agent can't send message: Kafka broker is not connected\n")
	}
}
