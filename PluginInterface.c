#include <stdio.h>
#include <dlfcn.h>
#include "PluginInterface.h"

int PluginInterface(const char* pluginPath, const char* dataPath,
                    const char* dataFormatPath, const char* resultFilePath)
{
    void *lib_handle = NULL;
    int (*PluginExec) (const char*, const char*, const char*);

    int result = -10;
    lib_handle = dlopen(pluginPath, RTLD_LAZY);
    if(lib_handle)
    {
        printf("dlopen returns %p\n", lib_handle);
        PluginExec = dlsym(lib_handle, "PluginExec");
        if(PluginExec)
        {
            printf("dlsym returns add = %p\n", PluginExec);
            result = PluginExec(dataPath, dataFormatPath, resultFilePath);
        }
        else
        {
            printf("Function entry not found in DLL: %s", dlerror());
        }
        dlclose(lib_handle);
    }
    else
    {
        printf("Unable to open DLL");
    }
    return result;
}