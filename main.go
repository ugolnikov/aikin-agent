package main

/*
	#cgo LDFLAGS: -lm
	#cgo CFLAGS: -g -Wall
	#include "PluginInterface.h"
*/
import "C"
import (
	"aikin-agent/broker"
	brokerInterface "aikin-agent/broker/interface"
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
)

var LOGFILE = "agent.log"
var agentLogger *log.Logger

func main() {
	httpServerExec()
}

// todo вынести pluginshandler в отдельный пакет, общий с aikin-server
func pluginsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		agentLogger.Println("uploadHandler: Wrong http-method: expected POST")
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	pluginName := r.Header.Get("PluginName")

	//todo сделать проверку на то, что плагин с таким именем уже загружался
	file, err := os.Create("./plugins/" + pluginName)
	defer file.Close()
	if err != nil {
		fmt.Println("Can not save plugin: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Add("Status", "Can not save plugin")
	}

	writer := bufio.NewWriter(file)
	writer.ReadFrom(r.Body)

	var scanner = bufio.NewScanner(r.Body)
	for scanner.Scan() {
		fmt.Printf("%q\n", scanner.Text())
	}
	err = scanner.Err()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Add("Status", "Bad data in request body")
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Add("Status", fmt.Sprintf("Plugin %s uploaded successfull\n", pluginName))
	fmt.Printf("Upload plugin %s from: %s\n", pluginName, r.Host)
}

func startHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		agentLogger.Println("startHandler: Wrong http-method: expected POST")
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	pluginName := r.Header.Get("PluginName")
	taskId := r.Header.Get("TaskId")

	// todo добавить автоматическое создание каталога data
	taskName := pluginName + taskId
	dataFile, err := os.Create("./plugins/data/" + taskName)
	defer dataFile.Close()
	if err != nil {
		fmt.Println("Can not save data: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Add("Status", "Can not save data")
	}

	dataFile.ReadFrom(r.Body)

	//todo добавить метод для конкатенации статусов в один заголовок
	w.Header().Add("Status", fmt.Sprintf("Data for task %s uploaded successfull\n", taskName))
	fmt.Printf("Uploaded data for task %s from: %s\n", taskName, r.Host) //todo некорректно отображается порт в адресе отправителя

	// todo добавить идентификатор агента к ответу
	w.Header().Add("Status", fmt.Sprintf("Agent 1: begining %s\n", taskName))

	path := "./plugins/"
	pluginPath := path + pluginName
	dataPath := path + "data/" + taskName
	dataFormatPath := "" //todo добавить поддержку dataFormat.json
	resultFilePath := path + "results/" + taskName + "result"

	//todo добавить enum для кодов статусов
	status := C.PluginInterface(C.CString(pluginPath), C.CString(dataPath),
		C.CString(dataFormatPath), C.CString(resultFilePath))
	//todo добавить switch по кодам статусов

	w.Header().Add("Status", fmt.Sprintf("Plugin returns status %d\n", status))
	resultFile, err := os.Open(resultFilePath)
	defer resultFile.Close()
	if err != nil {
		fmt.Printf("Can not open result file: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Add("Status", "Can not open result file")
		return
	}

	writer := bufio.NewWriter(w)
	writer.ReadFrom(resultFile)
}

func httpServerExec() {
	PORT := ":4001"
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Using default port number: ", PORT)
	} else {
		PORT = ":" + arguments[1]
	}

	http.HandleFunc("/plugins", pluginsHandler)
	http.HandleFunc("/start", startHandler)

	f, err := os.OpenFile(LOGFILE, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer f.Close()
	agentLogger = log.New(f, "customLogLineNumber ", log.LstdFlags)
	agentLogger.SetFlags(log.LstdFlags)

	var sender brokerInterface.Sender = broker.NewBroker("localhost:9091")
	sender.Send("Test message from agent")
	defer sender.Close()

	err = http.ListenAndServe(PORT, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}
